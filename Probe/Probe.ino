/*
 * Copyleft - VinCiv pour Gulliver - 2017
 *
 * Gnu GPL3 License
 * Code placé sous licence Gnu GPL3
 */
 
/**********************************************
// Transmit data from an Arduino card to another board
// This sketch sends a JSON string to a connected board through a custom software serial port
// Connections:
// * RX connected on pin 8 (connected to TX on the other board)
// * TX connected on pin 9 (connected to RX on the other board)
// * Do not forget to link ground of both boards (GND)
// First activate the emission board, then the reception board, by plugin them in this order
****************************************/

/**********************************************
// Transfert de données d'une carte arduino à une autre
// Emission des données  via le port Série Logiciel :
// Ce sketch envoie d'une chaine JSON à une carte connectée
// via le port série logiciel
// Branchements :
// * RX connecté sur pin 8 (connecté à TX sur l'autre carte)
// * TX connecté sur pin 9 (connect à RX sur l'autre carte)
// * Et ne pas oublier de relier les masses des 2 cartes (GND)
// Activer d'abord la carte d'émission, puis la réception en les rebranchant dans cet ordre
****************************************/

#include <SoftwareSerial.h>
#include <ArduinoJson.h>

int led = 13;
SoftwareSerial softwareSerial(8, 9); // RX, TX

char strJson[]= "{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";

void setup()
{
	// Define the software serial port 
	// Définition du port Série Logiciel
	softwareSerial.begin(57600);
	
	pinMode(led, OUTPUT);
}

// Send a message every 2 seconds
// Envoi d'un message toutes les 2 secondes
void loop()
{
	if (softwareSerial.available())
	{
		digitalWrite(led, HIGH);

		softwareSerial.write(strJson);
	}

	digitalWrite(led, LOW);

	// Wait for 2 seconds before looping
	// Attente de 2 secondes avant de recommencer
	delay(2000);
}
