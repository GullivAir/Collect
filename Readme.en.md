Gulliv'Air - Collect
===

## Présentation du projet

Gulliv'Air is a project of generic sensors [Gulliv'Air](https://gitlab.com/GullivAir/Doc).

Collect is the part regarding conception of box and collect of data from the sensors and the restitution for the bow owner.  

The project aims to propose several sensor boxes.  
Each one have its own technical spec., but all must be easily rebuilt.

## Documentation of the project

You can find the documentation of this project on the [Wiki](https://gitlab.com/GullivAir/Collect/wikis/home).

### Contributions

Gulliv'Air is a libre (open source) project, and we would be very glad to accept any contributions from the community.  
Please refer to [Contributions](https://gitlab.com/GullivAir/Doc/wikis/contributions) for more information.

## Licence

This project is under the GPLv3 license. For more information, please refer to [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

## Project structure

 - Probe: code on the Arduino to collect sensors data, and transmit them to other devices
 - WifiViewer: code on the ESP8266 to transmit the data (collected by the probes) to Wifi devices like smartphones or computers

## Requirements

### Arduino

 - [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
 - [ESP8266](https://github.com/esp8266/Arduino/)

## Help

[ESP8266 WiFi examples](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi/examples)

## Development

WifiViewer/test-server.js enables to test the WifiViewer/index.html client page
