// This is a simple node.js server to test index.html

var http = require('http');
var url = require('url');
var fs = require('fs');
var count = 0;

var server = http.createServer(function(req, res) {
    var page = url.parse(req.url).pathname;
    
    console.log(page);

    if (page == '/') {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.write(fs.readFileSync('index.html'));
    }
    else if (page == '/script.js' || page == '/style.css') {
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.write(fs.readFileSync(page.substr(1, page.length)));
    }
    else if (page == '/data/') {
        res.writeHead(200, {"Content-Type": "application/json"});
        data = {
            message: 'sensor data',
            count: count,
            latitude: 52.165000,
            longitude: -2.210000,
            temperature: 1,
            N02: 2,
            PM: 3
        }
        res.write(JSON.stringify(data));
        count++;
    }
    else {
        res.writeHead(404, {"Content-Type": "text/html"});
        res.write('This page does not exist.');
    }
    res.end();
});
server.listen(8080);