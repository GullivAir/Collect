/*
 * Copyleft - VinCiv pour Gulliver - 2017
 *
 * Gnu GPL3 License
 * Code placé sous licence Gnu GPL3
 */

// WifiServer is an access point which serves a simple html webpage to see the sensors data in real time
// An arduino sends sensors data through the custom serial port, the ESP8266 reads data and sends it to the connected web client (smartphone, computers)

// WifiServer est un point d'accès Wifi qui fournit une page web pour visualiser les données collectées par les capteurs
// Une carte Arduino envoie les données collectées par un port série logiciel, l'ESP8266 lit les données et les envoie aux client connectés (smartphone, ordinateurs)

// Todo:
// - save the html to serve on a file on the ESP memory?
// - enable the client to send the sensor data to an open database?

// WiFi libraries
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

// data format library
#include <ArduinoJson.h>

// managing server's files
#include "FS.h"

// data input from sensors module
#include <SoftwareSerial.h>

// WiFi parameters
const char *ssid = "GullivAir";
const char *password = "gulliver";

ESP8266WebServer server(80);

// data storage (tests)
char gMessage[256];
static int count = 0;
float   t = 21.3 ;
float   h = 52.6 ;
float   p = 983.15;
String  etatLed = "OFF";

// Receive the data from another pins, in order to let free the USB connection (for firmware upload)
//SoftwareSerial softwareSerial(8, 9); // RX, TX


String getPage(){
  String page = "<html lang=fr-FR><head><meta http-equiv='refresh' content='10'/>";
  page += "<title>ESP8266 - Essais pour le bo&icirc;ter de mesures de la qualit&eacute; de l'air - www.gulliver.eu.org</title>";
  page += "<style> body { background-color: #fffff; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }</style>";
  page += "</head><body><h1>ESP8266 - Essais pour le bo&icirc;ter de mesures de la qualit&eacute; de l'air</h1>";
  page += "<h3>DHT11</h3>";
  page += "<ul><li>Temp&eacute;rature : ";
  page += t;
  page += " &deg;C</li>";
  page += "<li>Humidit&eacute; : ";
  page += h;
  page += " %</li></ul><h3>BMP180</h3>";
  page += "<ul><li>Pression atmosph&eacute;rique : ";
  page += p;
  page += " mbar</li></ul>";
  page += "<h3>GPIO</h3>";
  page += "<form action='/' method='POST'>";
  page += "<ul><li>D13 (&eacute;tat : ";
  page += etatLed;
  page += ")";
  page += "<INPUT type='radio' name='LED' value='1'>ON";
  page += "<INPUT type='radio' name='LED' value='0'>OFF</li></ul>";
  page += "<INPUT type='submit' value='Actualiser'>";
  page += "<br><br><p><a hrf='www.gulliver.eu.org/capteurs_citoyens'>www.gulliver.eu.org/capteurs_citoyens</p>";
  page += "</body></html>";
  return page;
}

// Serves the main html file for the client interface (accessed from a smartphone for example)
// Todo: save the .html file in memory instead of hardcoding it?
// Fournis le fichier html pour l'interface client (accessible depuis un smartphone)
void handleRoot() {
  // File f = SPIFFS.open("/index-min.html", "r");
	// size_t size = f.size();
	// char buffer[size];
	// f.readBytes(buffer, size);
	// server.send(200, "text/html", buffer);
	// server.send(200, "text/html", f.readString());
	server.send(200, "text/html", getPage() );
}

// response to sensor data request (asked by the client javascript)
void handleDataRequest() {

	// Create a JSON object form the data
  //TODO: get the data from serial connection ....
	StaticJsonBuffer<200> jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();
	root["message"] = gMessage;
	root["count"] = ++count;
	root["latitude"] = 52.165000; //TODO
	root["longitude"] = -2.210000;
	root["temperature"] = 1;
	root["NO2"] = 2;
	root["PM"] = 3;

	size_t len = root.measureLength();
	size_t size = len+1;
	char json[size];
	root.printTo(json, sizeof(json));

	server.setContentLength (size);
	// server.sendHeader (const String &name, const String &value, bool first=false)
	// server.sendContent (json);
	server.send(200, "application/json", json);
}

// Start wifi hotshopt
void setup() {

	delay(1000);

 	// initialize serial connection
	Serial.begin(115200);

	// Start wifi hotspot
	IPAddress ESP_IP = WiFi.softAP(ssid, password);

	server.on("/", handleRoot);
	server.on("/data", handleDataRequest);

   // Connexion WiFi établie / WiFi connexion is OK
	Serial.println ( "" );
	Serial.print ( "Réseau Wifi : " ); Serial.println ( ssid );  
	Serial.print ( "Mdp :  " ); Serial.println ( password );
	Serial.print ( "Adresse IP : " ); Serial.println (ESP_IP);

	server.begin();
}

// Read data from software serial port (the connected arduino regularly sends sensor data)
// Serve html page
void loop() {
	
	// Read message from USB serial port for debug purpose
	int length = 256;
	char buffer[256];
	char character ='\n';
	length = Serial.readBytesUntil(character, buffer, length);
	if ( length > 0) {
		memcpy( gMessage, buffer, length);
		gMessage[length] = 0;
	}

	// Serve html page
	server.handleClient();

	delay(500);

	// Read data from software serial port (the connected arduino regularly sends sensor data)
	// Lit les données sur le port série logiciel (l'arduino envoie régulièrement des données)
	if (softwareSerial.available())
	{
		gMessage = softwareSerial.read();
		// Serial.write(softwareSerial.read());
	}
}
